package com.example.dell.tatkal;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegistrationActivity extends AppCompatActivity {
    private Button bRegister;
    private EditText etName, etGender, etEmail, etAddress, etNumber, etQualification, etProfession, etSkill, etExperience, etPassword;

    private FirebaseAuth yAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        etName = (EditText) findViewById(R.id.name_edittext);
        etGender = (EditText) findViewById(R.id.gender_edittext);
        etEmail = (EditText) findViewById(R.id.email_edittext);
        etAddress = (EditText) findViewById(R.id.address_edittext);
        etNumber = (EditText) findViewById(R.id.number_edittext);
        etQualification = (EditText) findViewById(R.id.qualification_edittext);
        etProfession = (EditText) findViewById(R.id.profession_edittext);
        etSkill = (EditText) findViewById(R.id.skill_edittext);
        etExperience = (EditText) findViewById(R.id.experience_edittext);
        etPassword = (EditText) findViewById(R.id.password_edittext_register);

        bRegister = (Button) findViewById(R.id.register_button);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                yAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()){
                            Toast.makeText(RegistrationActivity.this, "Signin error!", Toast.LENGTH_SHORT).show();
                        }else{
                            try {
                                String user_id = yAuth.getCurrentUser().getUid();
                                DatabaseReference user_db = FirebaseDatabase.getInstance().getReference("Users").child(user_id);

                                String name = etName.getText().toString();
                                String gender = etGender.getText().toString();
                                String email = etEmail.getText().toString();
                                String password = etPassword.getText().toString();
                                String address = etAddress.getText().toString();
                                String number = etNumber.getText().toString();
                                String qualification = etQualification.getText().toString();
                                String profession = etProfession.getText().toString();
                                String skill = etSkill.getText().toString();
                                String experience = etExperience.getText().toString();

                                Map newRecord = new HashMap();
                                newRecord.put("name", name);
                                newRecord.put("gender", gender);
                                newRecord.put("email", email);
                                newRecord.put("address", address);
                                newRecord.put("number", number);
                                newRecord.put("qualification", qualification);
                                newRecord.put("profession", profession);
                                newRecord.put("skill", skill);
                                newRecord.put("experience", experience);
                                newRecord.put("password", password);
//                            Intent intent = new Intent(RegistrationActivity.this, MainActivity.class);
//                            startActivity(intent);
//                            finish();
//                            return;

                                user_db.setValue(newRecord);
                            }catch (Exception e){}

                        }
                    }
                });

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        yAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        yAuth.removeAuthStateListener(firebaseAuthListener);
    }
}
